package id.ac.ub.restapi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import id.ac.ub.restapi.databinding.ActivityInsertBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InsertActivity extends AppCompatActivity {

    ActivityInsertBinding binding;
    ArrayList<Buku> listBuku = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
        binding = ActivityInsertBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        BukuAdapter adapter = new BukuAdapter(this, listBuku);
        PerpustakaanService ps = RetrofitClient.getClient().create(PerpustakaanService.class);
        binding.btins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<Buku> create =  ps.create(binding.ejudul.getText().toString(),binding.edeskripsi.getText().toString());
                create.enqueue(new Callback<Buku>() {
                    @Override
                    public void onResponse(Call<Buku> call, Response<Buku> response) {
                        if(response.isSuccessful()){
                            Log.d("Success", "nice");
                            adapter.notifyDataSetChanged();
                            finish();
                        } else{
                            Log.d("error", "ow" + response.errorBody());
                            Toast.makeText(InsertActivity.this, "Successfully added" + response.errorBody(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Buku> call, Throwable t) {
                        Log.d("error", "aw" + t.getMessage());
                        Toast.makeText(getApplicationContext(), "error : " + t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}